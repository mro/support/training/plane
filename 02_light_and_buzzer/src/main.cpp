/*
** Created on: 2024-06-24T23:15:18
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#include <Arduino.h>

#include "buzzer.hpp"
#include "ldr.hpp"

// Reference to our plane
LDR ldr;
Buzzer buzzer;

void setup() {
  // Initialize serial communication
  Serial.begin(9600);

  // Initialize buzzer
  buzzer.begin();
}

void loop() {
  // [2.4] TODO display the LED value (1 line missing)

  // [2.7] TODO: implement your application logic (2 lines missing)

}
