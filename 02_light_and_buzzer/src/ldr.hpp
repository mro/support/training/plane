/*
** Created on: 2024-06-24T23:15:18
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#ifndef _LDR__H_
#define _LDR__H_

#include <cstdint>
#include <Arduino.h>

class LDR {
public:
  /**
   * Gets light sensor raw ADC value
   * @return Light sensor in raw value
   */
  int getLightRaw();

  /**
   * Gets light sensor in percentage
   * @return Light sensor in percentage (0-100)
   */
  float getLight();

private:
  static const uint8_t LIGHT_SENSE_PIN; // Pin for the light sensor analog input
  static const int ADC_RANGE;           // ADC maximum value
};

#endif
