/*
** Created on: 2024-06-24T23:15:18
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#include "buzzer.hpp"
#include <Arduino.h>

// [2.5] TODO: find proper pin
const uint8_t Buzzer::BUZZER_PIN = 0;

Buzzer::Buzzer()
    : nextTick_{0}, beepCount_{-1}, beepFreq_{0}, beepTimeOn_{0},
      beepTimeOff_{0} {}

void Buzzer::begin() {
  // [2.5] TODO: set the proper buzzer pin mode (1 line missing)

}

bool Buzzer::loop() {
  if (beepCount_ > 0) {
    // Do a beep
    unsigned long now = millis();
    if (now >= nextTick_) {
      // [2.6] TODO: ring a bell ! (1 line missing)

      nextTick_ = now + beepTimeOn_ + beepTimeOff_;
      --beepCount_;
    }
  }
  return (beepCount_ > 0);
}

void Buzzer::beep(int count, uint16_t freq, uint16_t timeOn, uint16_t timeOff) {
  beepCount_ = count;
  beepFreq_ = freq;
  beepTimeOn_ = timeOn;
  beepTimeOff_ = timeOff;
}
