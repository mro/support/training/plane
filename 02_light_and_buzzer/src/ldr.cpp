/*
** Created on: 2024-06-24T23:15:18
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#include "ldr.hpp"

// [2.1] TODO: set proper pin !
const uint8_t LDR::LIGHT_SENSE_PIN = PIN_A0;
// [2.2] TODO: set proper ADC_RANGE !
const int LDR::ADC_RANGE = 128;

int LDR::getLightRaw() {
  // [2.3] TODO: acquire adc value (replace line below)
  return 0;
}

float LDR::getLight() {
  return (((float) getLightRaw()) / ADC_RANGE) * 100.0f;
}
