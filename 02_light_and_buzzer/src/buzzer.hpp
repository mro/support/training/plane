/*
** Created on: 2024-06-24T23:15:18
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#ifndef _BUZZER__H_
#define _BUZZER__H_

#include <cstdint>
#include <Arduino.h>

#include "pitches.h"

class Buzzer {
public:
  Buzzer();
  virtual ~Buzzer() = default;

  void begin();

  bool loop();

  void beep(int count = 1, uint16_t freq = NOTE_A4, uint16_t timeOn = 200,
            uint16_t timeOff = 200);

private:
  static const uint8_t BUZZER_PIN;

  unsigned long nextTick_;
  int beepCount_;
  uint16_t beepFreq_;
  uint16_t beepTimeOn_;
  uint16_t beepTimeOff_;
};

#endif
