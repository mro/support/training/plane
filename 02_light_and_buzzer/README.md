# Exercise 2 : light and buzzer

The objective of this exercise is to:
- use and configure IOs.
- understand ADCs and PWMs principles.
- connect components together.

For details about blocks used in those exercises, don't hesitate to refer to the
microcontroller's datasheet : [ATmega328P](https://ww1.microchip.com/downloads/aemDocuments/documents/MCU08/ProductDocuments/DataSheets/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf)

# Light and understanding ADCs

The lighting module uses a [PDV-P5001](https://www.advancedphotonix.com/wp-content/uploads/2015/07/DS-PDV-P5001.pdf), with a couple resistors connected to one of the chip's ADCs.

Details about ADC range and precision can be found in the [ATmega328P](https://ww1.microchip.com/downloads/aemDocuments/documents/MCU08/ProductDocuments/DataSheets/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf) datasheet.

Please refer to the [Arduino analogRead](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/) library function on how to acquire data.

## Things to do

- **[2.1]** The wrong `LIGHT_SENSE_PIN` is being used for the light sensor module, find and fix the pin declaration for this module ! (see [includes/ldr.h] and the plane schematic).
- **[2.2]** Check the `ADC_RANGE` it may also be wrong. 😵
- **[2.3]** Implement `LDR::getLightRaw` to read data value.
- **[2.4]** Test your driver by displaying value from the main loop (in [src/main.cpp](./src/main.cpp)).

# Buzzer and sound

The microcontroller being used is mounted on a [MakerNano](https://docs.google.com/document/d/1lUUJSzbsvzH_Ss6vDcHpGfVY7p_txuW5BZ1QOmHgcBA/view) board.

Some peripherals are directly mounted on this board, such as the usb-uart transceiver that we used in exercise 1.

Please refer to the [Arduino tone](https://www.arduino.cc/reference/en/language/functions/advanced-io/tone/) library function on how to use a PWM to play sound.

## Things to do

- **[2.5]** Identify the buzzer pin, set the correct `BUZZER_PIN` and configure it as an output.
- **[2.6]** Fix the `Buzzer::loop` code to properly ring a bell.
- **[2.7]** Constantly beep when it's dark ! 🔊 (see [src/main.cpp](src/main.cpp))


LDR is static -> make it homogenous
static const and constexpr in headers -> use only static in cpp

Add delay in loops

[Next Exercise](../03_bit_bang_leds/README.md)