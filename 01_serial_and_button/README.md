# Exercise 1 : serial and button

The objective of this exercise is to:
- setup a project
- communicate with the board
- interact with the hardware

# Building the project

1. Open this directory with VSCode/Codium, PlatformIO should automatically fetch required toolchains and libraries for the project.

2. The PlatformIO icon should appear, and you should be able to build the project:
![platformIO build](../docs/platform_io_build.png)

3. Connect your plane and click on "Upload", the firmware should be uploaded to
the device, and voilà.

# Navigating in the source

Even tho those exercises are C++ based, only a very basic knowledge of C++ is required, it is quite similar to Python (as seen in classrooms), and examples of code are available through those exercises.

Sources are all located in the `src` directory, split in modules, each module being implemented as a `class` split in a `cpp` and an `hpp` file (see [src/button.cpp](./src/button.cpp) and [src/button.hpp](src/button.hpp)).

For the sake of clarity and to facilitate the exercice most information is concentrated in `cpp` file.

Arduino SDK uses two functions that are always located in the [src/main.cpp](./src/main.cpp) file:
- `setup`: run once to setup all modules calling their `begin` function
- `loop`: called in loop by the SDK, likely to call your module `loop` function or to implement your application logic.

# The "Serial" communication

To communicate with the micro-controller a [serial port](https://en.wikipedia.org/wiki/Universal_asynchronous_receiver-transmitter)
or UART may be used.

The [MakerNano](https://docs.google.com/document/d/1lUUJSzbsvzH_Ss6vDcHpGfVY7p_txuW5BZ1QOmHgcBA/view) is fitted with a serial to usb converter.

This link is used to progam the micro-controller and also to communicate with user programs.

Please refer to the [Arduino Serial](https://www.arduino.cc/reference/en/language/functions/communication/serial/) library function (hint: ).

**Hint**: Have a look at `Serial::begin` function and its examples

## Things to do

- **[1.1]** Initialize the serial port at `9600` bits/s (or bauds) in `setup` function (see [src/main.cpp](src/main.cpp)).
- **[1.2]** At the end of the `setup` print a line with a message.

# Handling button presses

To read an input such as a button a pin must be configured as an input, eventually with a pullup depending on your hardware design.

Please refer to the [Arduino digitalRead](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalread/) library function along with its example use to configure the button.

Once configured the pin may be read, but reading a raw hardware pin is a bit shaky, a [debounce](https://en.wikipedia.org/wiki/Switch#Contact_bounce) is required to eliminate spurious presses.

This debounce should be implemented the following way:
- If no presses happen for more than `PRESS_TIMEOUT` then output number of presses and reset counter.
- If button press is detected (rising edge) and no press were detected for more than `DEBOUNCE_TIME` then count it as a keypress this is a keypress.

But this is only a partial implementation, let's fix this !

## Things to do

- **[1.3]** Initialize the button as an `INPUT_PULLUP` (to match your hardware design) in `Button::begin` function (see [src/button.cpp](src/button.cpp)).
- **[1.4]** Implement the `Button::getState` function to read the pin state (hint: button logic is inverted, use `!` operator to invert value in C++).
- **[1.5]** Fix the software debounce to actually count presses in `Button::loop`
- **[1.6]** Read the number of keypress detected in your main loop (see [src/main.cpp](src/main.cpp)) and display it.

[Next Exercise](../02_light_and_buzzer/README.md)