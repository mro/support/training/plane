/*
** Created on: 2024-06-24T17:46:09
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#include <Arduino.h>

#include "button.hpp"

unsigned long timer = 0;

Button button;

void setup() {
  // [1.1] TODO: Initialize serial communication at 9600 bauds (1 line missing)

  button.begin();

  // [1.2] TODO: Print a message that everything is ready (1 line missing)

}

void loop() {
  // This is our main loop
  int press = 0;

  // [1.6] TODO: Run the button loop to get the number of presses (1 line missing)


  if (press != 0)
  {
    Serial.println(press);
  }
}
