#ifndef _BUTTON__H_
#define _BUTTON__H_

#include <Arduino.h>

class Button
{
public:
    Button();
    virtual ~Button() = default;

    /**
     * Configure the button
    */
    void begin();

    /**
     * Loop, must be called as fast as possible
     * @return Number of press detected
    */
    int loop();

    /**
     * Gets immediate state of button
     * @return true if button is pressed
    */
    bool getState();

private:
    int nbPress_;               // Number of button press
    unsigned long lastPress_;   // Last time the button was pressed
    bool prevState_;            // Button previous state

    static constexpr unsigned long DEBOUNCE_TIME = 5;     // Ignore changes less this time
    static constexpr uint8_t BUTTON_PIN = 2;              // Button input pin
    static constexpr unsigned long PRESS_TIMEOUT = 500;   // Time before considering released
};


#endif
