#include "button.hpp"

Button::Button() : nbPress_{0}, lastPress_{0} {}

/**
 * Configure the button
 */
void Button::begin() {
  // [1.3] TODO: configure the button pin as an input (1 line missing)

}

bool Button::getState() {
  // [1.4] TODO: use read input button state (replace line below)
  return !0;
}

int Button::loop() {
  bool state = getState();
  unsigned long now = millis();

  if (((now - lastPress_) > PRESS_TIMEOUT) && (lastPress_ > 0)) {
    lastPress_ = 0;
    int ret = nbPress_;
    nbPress_ = 0;
    return ret;
  }

  if ((prevState_ != state) && state) {
    // Rising edge on button
    if ((now - lastPress_) > DEBOUNCE_TIME) {
      // [1.5 ]TODO: Take it into account


    }
  }
  prevState_ = state;
  return 0;
}
