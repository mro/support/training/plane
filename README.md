# Plane for students

This is the software project for the "MRO plane", that is based on a
[MakerNano](https://docs.google.com/document/d/1lUUJSzbsvzH_Ss6vDcHpGfVY7p_txuW5BZ1QOmHgcBA/view) board,
a [MPU-6050](https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Datasheet1.pdf) gyrometer,
a LDR (light sensor), a NTC and some smart leds.

Schematics are also available: [here](./docs/Projetavion.pdf)

## Software

The microcontroler software is based on the [Arduino](https://www.arduino.cc/) framework.

## Getting Started

### Prerequisites

Some knowledge of [C/C++](https://en.wikipedia.org/wiki/C%2B%2B) programming language are required (Arduino flavored, see [Arduino Language Reference](https://www.arduino.cc/reference/en/)).

In order to build (and flash) the firmare, these tools are required:
- [Visual Studio Code](https://code.visualstudio.com/) or [Codium](https://vscodium.com/)
- [PlatformIO extension](https://marketplace.visualstudio.com/items?itemName=platformio.platformio-ide) for Visual Studio

## Exercises

To get started with each exercise, open each exercise folder with VSCode/Codium.

**Note:** each exercise is a separate project : do close and restart VSCode/Codium in-between exercises.

The following exercises are available:
1. [Serial and button](./01_serial_and_button/README.md)
2. [Light and buzzer](./02_light_and_buzzer/README.md)
3. [Bit bang LEDs](./03_bit_bang_leds/README.md)
4. [Gyroscope](./04_gyroscope/README.md)

## Once you're done with exercises

A final [solution](./solution/README.md), but please refer to it only in case of extreme distress 😱

Feeling adventuruous you can also do this complete set of exercises
going baremetal (not using the Arduino SDK), an example implementation is available
[here](https://gitlab.cern.ch/mro/support/training/cmake-ci-tdd-example).