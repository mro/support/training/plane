# Exercise 4 : Gyroscope

The objective of this exercise is to:
- work and understand a more complex peripheral
- refer to external documentation

# Gyroscope

The plane hardware has an integrated accelerometer/gyroscope, a [MPU6050 datasheet](https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Datasheet1.pdf) (and its [registers map document](https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Register-Map1.pdf)).

To use this component we decided to use an available driver: [MPU6050_light](https://github.com/rfetick/MPU6050_light).

**Hint:** on the driver repository there's a nice pdf, check on the "Data Getters" chapter.

**Hint:** in the datasheet there's a diagram showing axis orientation (page 21).

## Things to do

1. Find and review the MPU6050 driver documentation.
2. Implement `LEDAnimations::loop` to light-up the leds according to your plane
attitude:
- front led must be red when going down and green when going up
- left led must be green when turning left and red when turning right
- right led must be green when turning right and red when turning left

## Once implemented

You're done with exercises, congratulations !!!! 🎉🎉🎉

You can now have a look to [solution](../solution/).