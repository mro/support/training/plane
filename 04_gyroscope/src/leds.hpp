/*
** Created on: 2024-06-24T23:15:18
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#ifndef _LEDS__H_
#define _LEDS__H_

#include <Arduino.h>
#include <FAB_LED.h>


//Template for RGB leds
#define FAB_TVAR_WS2812B WS2812B_1H_CY, WS2812B_1L_CY, WS2812B_0H_CY, \
	WS2812B_0L_CY, WS2812B_MS_REFRESH, dataPortId, dataPortBit, A, 0, RGB, ONE_PORT_BITBANG
template<avrLedStripPort dataPortId, uint8_t dataPortBit>
class ws2812b2 : public avrBitbangLedStrip<FAB_TVAR_WS2812B>
{
	public:
	ws2812b2() : avrBitbangLedStrip<FAB_TVAR_WS2812B>() {};
	~ws2812b2() {};
};
#undef FAB_TVAR_WS2812B


/**
 * Class for managing LEDs on the plane
*/
class LEDs {
public:
    LEDs();
    virtual ~LEDs() = default;
    /**
     * Configures LEDs
    */
    void begin();

    /**
     * Sets one Led
     * @param number Number of the LED (0 based)
     * @param red Red value (0-255)
     * @param green Green value (0-255)
     * @param blue Blue value (0-255)
     * @param sendNow True will send immediatly to hardware
    */
    void setLed(uint8_t number, uint8_t red, uint8_t green, uint8_t blue, bool sendNow = true);

    /**
     * Sets all Leds
     * @param red Red value (0-255)
     * @param green Green value (0-255)
     * @param blue Blue value (0-255)
     * @param sendNow True will send immediatly to hardware
    */
    void setLeds(uint8_t red, uint8_t green, uint8_t blue, bool sendNow = true);

    /**
     * Sets a LED in HSV color space
     * @param number Number of the LED (0 based)
     * @param Hue value (0 -> 360 degree)
     * @param Saturation (0 -> 1)
     * @param Value (0 -> 1)
     * @param sendNow True will send immediatly to hardware
    */
    void setLedHSV(uint8_t number, float hue, float saturation, float value, bool sendNow = true);

    /**
     * Sets all LEDs in HSV color space
     * @param Hue value (0 -> 360 degree)
     * @param Saturation (0 -> 1)
     * @param Value (0 -> 1)
     * @param sendNow True will send immediatly to hardware
    */
    void setLedsHSV(float hue, float saturation, float value, bool sendNow = true);

    /**
     * Send leds value to hardware
    */
    void updateLeds();

    /**
     * Gets number of leds
     * @return Number of leds usable on the plane
    */
    inline constexpr uint8_t ledsCount() { return THT_LEDS_COUNT + SMD_LEDS_COUNT; }

    static constexpr uint8_t LD1 = 0;   // Index of LD1 (SMD front LED)
    static constexpr uint8_t LD2 = 1;   // Index of LD2 (SMD left wing LED)
    static constexpr uint8_t LD3 = 2;   // Index of LD3 (SMD right wing LED)
    static constexpr uint8_t LD4 = 3;   // Index of LD4 (THT front LED)
    static constexpr uint8_t LD5 = 4;   // Index of LD5 (THT left tailplane)
    static constexpr uint8_t LD6 = 5;   // Index of LD6 (THT right tailplane)

private:
    static constexpr int THT_LEDS_COUNT = 3;        // Number of THT LEDs
    static constexpr int SMD_LEDS_COUNT = 3;        // Number of SMD LEDs
    ws2812b<B,3> smdLed_;       // SMD leds on port B3 (RGB)
    ws2812b2<B,4> thtLed_;       // THT leds on port B4 (GBR)
    rgb LEDs_[THT_LEDS_COUNT + SMD_LEDS_COUNT];  // leds data
    bool smdLedsDirty_;         // Needs to send SMD leds
    bool thtLedsDirty_;         // Needs to send THT leds

    /**
     * Convert HSV to RGB
     * @param hue Hue value
     * @param saturationS Saturation value
     * @param value Value
    */
    static void HSVToRGB(float hue, float saturation, float value, uint8_t& red, uint8_t& green, uint8_t& blue);

};

#endif
