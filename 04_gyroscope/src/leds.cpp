/*
** Created on: 2024-06-24T23:15:18
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#include "leds.hpp"

LEDs::LEDs() : LEDs_{}, smdLedsDirty_{false}, thtLedsDirty_{false}
{
}

void LEDs::begin()
{
    thtLed_.clear(THT_LEDS_COUNT * 2);      //Clears LED
    smdLed_.clear(SMD_LEDS_COUNT * 2);
}

void LEDs::setLed(uint8_t number, uint8_t red, uint8_t green, uint8_t blue, bool sendNow)
{
    if((number >= 0) && (number < ledsCount())){
        bool changed = false;
        if(LEDs_[number].r != red){
            changed = true;
            LEDs_[number].r = red;
        }

        if(LEDs_[number].g != green){
            changed = true;
            LEDs_[number].g = green;
        }

        if(LEDs_[number].b != blue){
            changed = true;
            LEDs_[number].b = blue;
        }

        if(number < SMD_LEDS_COUNT){
            smdLedsDirty_ = changed ? true : smdLedsDirty_;
        }else{
            thtLedsDirty_ = changed ? true : thtLedsDirty_;
        }

        if(sendNow){
            updateLeds();
        }
    }
}

void LEDs::setLeds(uint8_t red, uint8_t green, uint8_t blue, bool sendNow)
{
    for(uint8_t i=0; i<ledsCount(); ++i){
        setLed(i, red, green, blue, false);
    }
    if(sendNow){
        updateLeds();
    }
}

void LEDs::setLedHSV(uint8_t number, float hue, float saturation, float value, bool sendNow)
{
    uint8_t r,g,b = 0;
    HSVToRGB(hue, saturation, value, r, g, b);
    setLed(number, r, g, b, sendNow);
}

void LEDs::setLedsHSV(float hue, float saturation, float value, bool sendNow)
{
    uint8_t r,g,b = 0;
    HSVToRGB(hue, saturation, value, r, g, b);
    setLeds(r, g, b, sendNow);
}

void LEDs::updateLeds()
{
    if(smdLedsDirty_){
        smdLedsDirty_ = false;
        smdLed_.sendPixels(SMD_LEDS_COUNT, LEDs_);
    }
    if(thtLedsDirty_){
        thtLedsDirty_ = false;
        thtLed_.sendPixels(THT_LEDS_COUNT, LEDs_ + SMD_LEDS_COUNT);
    }
}

void LEDs::HSVToRGB(float hue, float saturation, float value, uint8_t& red, uint8_t& green, uint8_t& blue)
{
    float r = 0, g = 0, b = 0;
	if (saturation == 0)
	{
		r = value;
		g = value;
		b = value;
	}
	else
	{
		int i;
		double f, p, q, t;

		if (hue == 360)
			hue = 0;
		else
			hue = hue / 60;

		i = (int)trunc(hue);
		f = hue - i;

		p = value * (1.0 - saturation);
		q = value * (1.0 - (saturation * f));
		t = value * (1.0 - (saturation * (1.0 - f)));

		switch (i)
		{
		case 0:
			r = value;
			g = t;
			b = p;
			break;

		case 1:
			r = q;
			g = value;
			b = p;
			break;

		case 2:
			r = p;
			g = value;
			b = t;
			break;

		case 3:
			r = p;
			g = q;
			b = value;
			break;

		case 4:
			r = t;
			g = p;
			b = value;
			break;

		default:
			r = value;
			g = p;
			b = q;
			break;
		}

	}

	red = (uint8_t)(r * 255);
	green = (uint8_t)(g * 255);
	blue = (uint8_t)(b * 255);
}
