/*
** Created on: 2024-06-24T23:15:18
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#include "leds.hpp"
#include "leds_animations.hpp"
#include <Arduino.h>

#include "Wire.h"
#include <MPU6050_light.h>

MPU6050 mpu(Wire);

// Reference to our plane
LEDs leds;
LEDAnimations ledAnims(leds, mpu);

/**
 * Error state
 */
void error(const char *msg) {
  Serial.println(msg);
  while (1) {
    delay(1000);
  }
}

void setup() {
  // Initialize serial communication
  Serial.begin(9600);
  // Starts I2C for MPU
  Wire.begin();
  // Initialize LEDs
  leds.begin();
  // Initialize MPU and go to error state if not found
  byte status = mpu.begin(3);
  if (status != 0)
    error("Failed to initalize MPU6050");

  mpu.upsideDownMounting =
      true; // uncomment this line if the MPU6050 is mounted upside-down
  mpu.calcOffsets(); // gyro and accelero

  // Initialize LED animations
  ledAnims.begin();
}

unsigned long lastTempRead_ = 0;

void loop() {
  // //Fetch data from accelerometer
  mpu.update();

  // Loop the led animation
  ledAnims.loop();
}
