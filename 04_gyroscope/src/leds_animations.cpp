/*
** Created on: 2024-06-24T23:15:18
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#include "leds_animations.hpp"
#include <Arduino.h>

LEDAnimations::LEDAnimations(LEDs &leds, MPU6050 &mpu)
    : leds_{leds}, mpu_(mpu), prevMPURead_{0} {}

void LEDAnimations::begin() {}

void LEDAnimations::loop() {
  unsigned long now = millis();

  if ((now - prevMPURead_) >= mpuReadPeriod) {
    // TODO : implement your leds blinking program
  }

  leds_.updateLeds();
}
