/*
** Created on: 2024-06-24T23:15:18
** Author: Mathieu Donze <mathieu.donze@cern.ch>
*/

#ifndef _LEDS_ANIMATIONS__H_
#define _LEDS_ANIMATIONS__H_

#include "leds.hpp"
#include <MPU6050_light.h>

class LEDAnimations {
public:
  LEDAnimations(LEDs &leds, MPU6050 &mpu);
  virtual ~LEDAnimations() = default;
  void begin();
  void loop();

private:
  LEDs &leds_;                // Reference to our leds
  MPU6050 &mpu_;              // Reference to the MPU6050
  unsigned long prevMPURead_; // Previous MPU read time

  static constexpr unsigned long mpuReadPeriod = 100; // MPU update period
};

#endif
