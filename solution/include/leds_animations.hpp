#ifndef _LEDS_ANIMATIONS__H_
#define _LEDS_ANIMATIONS__H_

#include "leds.hpp"
#include <MPU6050_light.h>

class LEDAnimations
{
public:
    LEDAnimations(LEDs& leds, MPU6050& mpu);
    virtual ~LEDAnimations() = default;
    void begin();
    void loop();

    inline bool wingsEnabled() { return wingsEnabled_; }
    inline bool ldrEnabled() { return ldrEnabled_; }
    inline bool mpuEnabled() { return mpuEnabled_; }
    inline bool temperatureEnabled() { return tempEnabled_; }

    inline void setWingsEnabled(bool enabled ) { wingsEnabled_ = enabled; }
    inline void setLDREnabled(bool enabled ) { ldrEnabled_ = enabled; }
    inline void setMPUEnabled(bool enabled ) { mpuEnabled_ = enabled; }
    inline void setTemperatureEnabled(bool enabled ) { tempEnabled_ = enabled; }
private:
    LEDs& leds_;                // Reference to our leds
    MPU6050& mpu_;              // Reference to the MPU6050
    unsigned long prevFlash_;   // Previous time of flash
    unsigned long prevLDRRead_; // Previous LDR read time
    unsigned long prevMPURead_; // Previous MPU read time
    bool wingsEnabled_;         // Wings animation is enabled
    bool ldrEnabled_;           // LDR animation is enabled
    bool mpuEnabled_;           // MPU animations are enabled
    bool tempEnabled_;          // Temperature enabled
    static constexpr unsigned long flashDuration = 100;  // Duration of the flash
    static constexpr unsigned long flashPeriod = 1000;  // Time between flashes
    static constexpr uint8_t wingsBrightness = 180;      // Brightness of the wings

    static constexpr unsigned long ldrReadPeriod = 50;  // LDR reading period
    static constexpr float ldrThreshold = 45;           // LDR threshold for switching LED off

    static constexpr unsigned long mpuReadPeriod = 100; // MPU update period

    static constexpr float temperatureThreshold = 30;   // Temperature threshold for putting all leds red
};


#endif
