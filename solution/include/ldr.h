#ifndef _LDR__H_
#define _LDR__H_

#include <Arduino.h>

class LDR
{
public:

    /**
     * Gets light sensor raw ADC value
     * @return Light sensor in raw value
    */
    inline static int getLightRaw() { return analogRead(LIGHT_SENSE_PIN); }

    /**
     * Gets light sensor in percentage
     * @return Light sensor in percentage (0-100)
    */
    float static getLight();

private:
    static constexpr uint8_t LIGHT_SENSE_PIN = PIN_A6;   //Pin for the light sensor analog input
    static constexpr int ADC_RANGE = 1024;               // ADC maximum value

    LDR() = delete;
    virtual ~LDR() = delete;

};


#endif
