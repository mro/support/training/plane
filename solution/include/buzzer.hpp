#ifndef _BUZZER__H_
#define _BUZZER__H_

#include <Arduino.h>
#include "pitches.h"

class Buzzer
{
public:
    Buzzer();
    virtual ~Buzzer() = default;

    void begin();

    bool loop();

    void playNext();

    void play(uint8_t index);

    void beep(int count=1, uint16_t freq = 440, uint16_t timeOn = 200, uint16_t timeOff = 200);

    void stop();

private:
    typedef struct { uint16_t note; int16_t duration; } melody_note_t;

    typedef struct { const melody_note_t* melody; size_t lenght; int tempo; } melody_t;

    static constexpr melody_note_t marioMelody[] PROGMEM = {
        {NOTE_E7, 24}, {NOTE_E7, 24}, {0, 24}, {NOTE_E7, 24},
        {0, 24}, {NOTE_C7, 24}, {NOTE_E7, 24}, {0, 24},
        {NOTE_G7, 24}, {0, 24}, {0, 24}, {0, 24},
        {NOTE_G6, 24}, {0, 24}, {0, 24}, {0, 24},

        {NOTE_C7, 24}, {0, 24}, {0, 24}, {NOTE_G6, 24},
        {0, 24}, {0, 24}, {NOTE_E6, 24}, {0, 24},
        {0, 24}, {NOTE_A6, 24}, {0, 24}, {NOTE_B6, 24},
        {0, 24}, {NOTE_AS6, 24}, {NOTE_A6, 24}, {0, 24},

        {NOTE_G6, 18}, {NOTE_E7, 18}, {NOTE_G7, 18},
        {NOTE_A7, 24}, {0, 24}, {NOTE_F7, 24},
        {NOTE_G7, 24}, {0, 24}, {NOTE_E7, 24},
        {0, 24}, {NOTE_C7, 24}, {NOTE_D7, 24},
        {NOTE_B6, 24}, {0, 24}, {0, 24},

        {NOTE_C7, 24}, {0, 24}, {0, 24}, {NOTE_G6, 24},
        {0, 24}, {0, 24}, {NOTE_E6, 24}, {0, 24},
        {0, 24}, {NOTE_A6, 24}, {0, 24}, {NOTE_B6, 24},
        {0, 24}, {NOTE_AS6, 24}, {NOTE_A6, 24}, {0, 24},

        {NOTE_G6, 18}, {NOTE_E7, 18}, {NOTE_G7, 18},
        {NOTE_A7, 24}, {0, 24}, {NOTE_F7, 24},
        {NOTE_G7, 24}, {0, 24}, {NOTE_E7, 24},
        {0, 24}, {NOTE_C7, 24}, {NOTE_D7, 24},
        {NOTE_B6, 24}, {0, 24}, {0, 24},
    };

    static constexpr melody_note_t starWarsMelody[] PROGMEM = {
        {NOTE_AS4,8}, {NOTE_AS4,8}, {NOTE_AS4,8},//1
        {NOTE_F5,2}, {NOTE_C6,2},
        {NOTE_AS5,8}, {NOTE_A5,8}, {NOTE_G5,8}, {NOTE_F6,2}, {NOTE_C6,4},
        {NOTE_AS5,8}, {NOTE_A5,8}, {NOTE_G5,8}, {NOTE_F6,2}, {NOTE_C6,4},
        {NOTE_AS5,8}, {NOTE_A5,8}, {NOTE_AS5,8}, {NOTE_G5,2}, {NOTE_C5,8}, {NOTE_C5,8}, {NOTE_C5,8},
        {NOTE_F5,2}, {NOTE_C6,2},
        {NOTE_AS5,8}, {NOTE_A5,8}, {NOTE_G5,8}, {NOTE_F6,2}, {NOTE_C6,4},

        {NOTE_AS5,8}, {NOTE_A5,8}, {NOTE_G5,8}, {NOTE_F6,2}, {NOTE_C6,4}, //8
        {NOTE_AS5,8}, {NOTE_A5,8}, {NOTE_AS5,8}, {NOTE_G5,2}, {NOTE_C5,-8}, {NOTE_C5,16},
        {NOTE_D5,-4}, {NOTE_D5,8}, {NOTE_AS5,8}, {NOTE_A5,8}, {NOTE_G5,8}, {NOTE_F5,8},
        {NOTE_F5,8}, {NOTE_G5,8}, {NOTE_A5,8}, {NOTE_G5,4}, {NOTE_D5,8}, {NOTE_E5,4}, {NOTE_C5,-8}, {NOTE_C5,16},
        {NOTE_D5,-4}, {NOTE_D5,8}, {NOTE_AS5,8}, {NOTE_A5,8}, {NOTE_G5,8}, {NOTE_F5,8},

        {NOTE_C6,-8}, {NOTE_G5,16}, {NOTE_G5,2}, {REST,8}, {NOTE_C5,8}, //13
        {NOTE_D5,-4}, {NOTE_D5,8}, {NOTE_AS5,8}, {NOTE_A5,8}, {NOTE_G5,8}, {NOTE_F5,8},
        {NOTE_F5,8}, {NOTE_G5,8}, {NOTE_A5,8}, {NOTE_G5,4}, {NOTE_D5,8}, {NOTE_E5,4}, {NOTE_C6,-8}, {NOTE_C6,16},
        {NOTE_F6,4}, {NOTE_DS6,8}, {NOTE_CS6,4}, {NOTE_C6,8}, {NOTE_AS5,4}, {NOTE_GS5,8}, {NOTE_G5,4}, {NOTE_F5,8},
        {NOTE_C6,1},
    };

    static constexpr melody_note_t happyBirthdayMelody[] PROGMEM = {
        {NOTE_D5, 8}, {NOTE_D5, 8}, {NOTE_E5, 4}, {NOTE_D5, 4}, {NOTE_G5, 4},
        {NOTE_FS5, 2},{NOTE_D5, 8}, {NOTE_D5, 8}, {NOTE_E5, 4}, {NOTE_D5, 4},
        {NOTE_A5, 4}, {NOTE_G5, 2}, {NOTE_D5, 8}, {NOTE_D5, 8}, {NOTE_D6, 4},
        {NOTE_B5, 4}, {NOTE_G5, 4}, {NOTE_FS5, 4},{NOTE_E5, 4}, {NOTE_C6, 8},
        {NOTE_C6, 8}, {NOTE_B5, 4}, {NOTE_G5, 4}, {NOTE_A5, 4}, {NOTE_G5, 2},
    };

    static constexpr melody_note_t zeldaMelody[] PROGMEM = {
        {NOTE_AS4, -2}, {NOTE_F4, 8}, {NOTE_F4, 8}, {NOTE_AS4, 8},//1
        {NOTE_GS4, 16},  {NOTE_FS4, 16}, {NOTE_GS4, -2},
        {NOTE_AS4, -2},  {NOTE_FS4, 8},  {NOTE_FS4, 8}, {NOTE_AS4, 8},
        {NOTE_A4, 16},  {NOTE_G4, 16},  {NOTE_A4, -2},
        {0,1},

        {NOTE_AS4, 4},  {NOTE_F4, -4},  {NOTE_AS4, 8},  {NOTE_AS4, 16},  {NOTE_C5, 16}, {NOTE_D5, 16}, {NOTE_DS5, 16},//7
        {NOTE_F5, 2},  {NOTE_F5, 8},  {NOTE_F5, 8}, {NOTE_F5, 8}, {NOTE_FS5, 16}, {NOTE_GS5, 16},
        {NOTE_AS5, -2}, {NOTE_AS5, 8}, {NOTE_AS5, 8}, {NOTE_GS5, 8}, {NOTE_FS5, 16},
        {NOTE_GS5, -8}, {NOTE_FS5, 16}, {NOTE_F5, 2}, {NOTE_F5, 4},

        {NOTE_DS5,-8}, {NOTE_F5,16}, {NOTE_FS5,2}, {NOTE_F5,8}, {NOTE_DS5,8}, //11
        {NOTE_CS5,-8}, {NOTE_DS5,16}, {NOTE_F5,2}, {NOTE_DS5,8}, {NOTE_CS5,8},
        {NOTE_C5,-8}, {NOTE_D5,16}, {NOTE_E5,2}, {NOTE_G5,8},
        {NOTE_F5,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,8}, {NOTE_F4,16}, {NOTE_F4,8},

        {NOTE_AS4,4}, { NOTE_F4,-4}, { NOTE_AS4,8}, { NOTE_AS4,16}, { NOTE_C5,16}, {NOTE_D5,16}, {NOTE_DS5,16},//15
        {NOTE_F5,2}, { NOTE_F5,8}, { NOTE_F5,8}, { NOTE_F5,8}, { NOTE_FS5,16}, {NOTE_GS5,16},
        {NOTE_AS5,-2}, {NOTE_CS6,4},
        {NOTE_C6,4}, {NOTE_A5,2}, {NOTE_F5,4},
        {NOTE_FS5,-2}, {NOTE_AS5,4},
        {NOTE_A5,4}, {NOTE_F5,2}, {NOTE_F5,4},

        {NOTE_FS5,-2}, {NOTE_AS5,4},
        {NOTE_A5,4}, {NOTE_F5,2}, {NOTE_D5,4},
        {NOTE_DS5,-2}, {NOTE_FS5,4},
        {NOTE_F5,4}, {NOTE_CS5,2}, {NOTE_AS4,4},
        {NOTE_C5,-8}, {NOTE_D5,16}, {NOTE_E5,2}, {NOTE_G5,8},
        {NOTE_F5,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,16}, {NOTE_F4,8}, {NOTE_F4,16}, {NOTE_F4,8},
    };

    static constexpr melody_note_t tetrisMelody[] PROGMEM = {
        {NOTE_E5,4}, {NOTE_B4,8}, {NOTE_C5,8}, {NOTE_D5,4}, {NOTE_C5,8}, {NOTE_B4,8},
        {NOTE_A4,4}, {NOTE_A4,8}, {NOTE_C5,8}, {NOTE_E5,4}, {NOTE_D5,8}, {NOTE_C5,8},
        {NOTE_B4,-4}, {NOTE_C5,8}, {NOTE_D5,4}, {NOTE_E5,4},
        {NOTE_C5,4}, {NOTE_A4,4}, {NOTE_A4,8}, {NOTE_A4,4}, {NOTE_B4,8}, {NOTE_C5,8},

        {NOTE_D5,-4}, {NOTE_F5,8}, {NOTE_A5,4}, {NOTE_G5,8}, {NOTE_F5,8},
        {NOTE_E5,-4}, {NOTE_C5,8}, {NOTE_E5,4}, {NOTE_D5,8}, {NOTE_C5,8},
        {NOTE_B4,4}, {NOTE_B4,8}, {NOTE_C5,8}, {NOTE_D5,4}, {NOTE_E5,4},
        {NOTE_C5,4}, {NOTE_A4,4}, {NOTE_A4,4}, {REST,4},

        {NOTE_E5,4}, {NOTE_B4,8}, {NOTE_C5,8}, {NOTE_D5,4}, {NOTE_C5,8}, {NOTE_B4,8},
        {NOTE_A4,4}, {NOTE_A4,8}, {NOTE_C5,8}, {NOTE_E5,4}, {NOTE_D5,8}, {NOTE_C5,8},
        {NOTE_B4,-4}, {NOTE_C5,8}, {NOTE_D5,4}, {NOTE_E5,4},
        {NOTE_C5,4}, {NOTE_A4,4}, {NOTE_A4,8}, {NOTE_A4,4}, {NOTE_B4,8}, {NOTE_C5,8},

        {NOTE_D5,-4}, {NOTE_F5,8}, {NOTE_A5,4}, {NOTE_G5,8}, {NOTE_F5,8},
        {NOTE_E5,-4}, {NOTE_C5,8}, {NOTE_E5,4}, {NOTE_D5,8}, {NOTE_C5,8},
        {NOTE_B4,4}, {NOTE_B4,8}, {NOTE_C5,8}, {NOTE_D5,4}, {NOTE_E5,4},
        {NOTE_C5,4}, {NOTE_A4,4}, {NOTE_A4,4}, {REST,4},


        {NOTE_E5,2}, {NOTE_C5,2},
        {NOTE_D5,2}, {NOTE_B4,2},
        {NOTE_C5,2}, {NOTE_A4,2},
        {NOTE_GS4,2}, {NOTE_B4,4}, {REST,8},
        {NOTE_E5,2}, {NOTE_C5,2},
        {NOTE_D5,2}, {NOTE_B4,2},
        {NOTE_C5,4}, {NOTE_E5,4}, {NOTE_A5,2},
        {NOTE_GS5,2},
    };

    static constexpr melody_note_t nevergonnagiveyouupMelody[] PROGMEM = {
        {NOTE_D5,-4}, {NOTE_E5,-4}, {NOTE_A4,4},//1
        {NOTE_E5,-4}, {NOTE_FS5,-4}, {NOTE_A5,16}, {NOTE_G5,16}, {NOTE_FS5,8},
        {NOTE_D5,-4}, {NOTE_E5,-4}, {NOTE_A4,2},
        {NOTE_A4,16}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,8}, {NOTE_D5,16},
        {NOTE_D5,-4}, {NOTE_E5,-4}, {NOTE_A4,4},//repeatfrom1
        {NOTE_E5,-4}, {NOTE_FS5,-4}, {NOTE_A5,16}, {NOTE_G5,16}, {NOTE_FS5,8},
        {NOTE_D5,-4}, {NOTE_E5,-4}, {NOTE_A4,2},
        {NOTE_A4,16}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,8}, {NOTE_D5,16},
        {REST,4}, {NOTE_B4,8}, {NOTE_CS5,8}, {NOTE_D5,8}, {NOTE_D5,8}, {NOTE_E5,8}, {NOTE_CS5,-8},
        {NOTE_B4,16}, {NOTE_A4,2}, {REST,4},

        {REST,8}, {NOTE_B4,8}, {NOTE_B4,8}, {NOTE_CS5,8}, {NOTE_D5,8}, {NOTE_B4,4}, {NOTE_A4,8},//7
        {NOTE_A5,8}, {REST,8}, {NOTE_A5,8}, {NOTE_E5,-4}, {REST,4},
        {NOTE_B4,8}, {NOTE_B4,8}, {NOTE_CS5,8}, {NOTE_D5,8}, {NOTE_B4,8}, {NOTE_D5,8}, {NOTE_E5,8}, {REST,8},
        {REST,8}, {NOTE_CS5,8}, {NOTE_B4,8}, {NOTE_A4,-4}, {REST,4},
        {REST,8}, {NOTE_B4,8}, {NOTE_B4,8}, {NOTE_CS5,8}, {NOTE_D5,8}, {NOTE_B4,8}, {NOTE_A4,4},
        {NOTE_E5,8}, {NOTE_E5,8}, {NOTE_E5,8}, {NOTE_FS5,8}, {NOTE_E5,4}, {REST,4},

        {NOTE_D5,2}, {NOTE_E5,8}, {NOTE_FS5,8}, {NOTE_D5,8},//13
        {NOTE_E5,8}, {NOTE_E5,8}, {NOTE_E5,8}, {NOTE_FS5,8}, {NOTE_E5,4}, {NOTE_A4,4},
        {REST,2}, {NOTE_B4,8}, {NOTE_CS5,8}, {NOTE_D5,8}, {NOTE_B4,8},
        {REST,8}, {NOTE_E5,8}, {NOTE_FS5,8}, {NOTE_E5,-4}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_FS5,-8}, {NOTE_FS5,-8}, {NOTE_E5,-4}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},

        {NOTE_E5,-8}, {NOTE_E5,-8}, {NOTE_D5,-8}, {NOTE_CS5,16}, {NOTE_B4,-8}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},//18
        {NOTE_D5,4}, {NOTE_E5,8}, {NOTE_CS5,-8}, {NOTE_B4,16}, {NOTE_A4,8}, {NOTE_A4,8}, {NOTE_A4,8},
        {NOTE_E5,4}, {NOTE_D5,2}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_FS5,-8}, {NOTE_FS5,-8}, {NOTE_E5,-4}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_A5,4}, {NOTE_CS5,8}, {NOTE_D5,-8}, {NOTE_CS5,16}, {NOTE_B4,8}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},

        {NOTE_D5,4}, {NOTE_E5,8}, {NOTE_CS5,-8}, {NOTE_B4,16}, {NOTE_A4,4}, {NOTE_A4,8},//23
        {NOTE_E5,4}, {NOTE_D5,2}, {REST,4},
        {REST,8}, {NOTE_B4,8}, {NOTE_D5,8}, {NOTE_B4,8}, {NOTE_D5,8}, {NOTE_E5,4}, {REST,8},
        {REST,8}, {NOTE_CS5,8}, {NOTE_B4,8}, {NOTE_A4,-4}, {REST,4},
        {REST,8}, {NOTE_B4,8}, {NOTE_B4,8}, {NOTE_CS5,8}, {NOTE_D5,8}, {NOTE_B4,8}, {NOTE_A4,4},
        {REST,8}, {NOTE_A5,8}, {NOTE_A5,8}, {NOTE_E5,8}, {NOTE_FS5,8}, {NOTE_E5,8}, {NOTE_D5,8},

        {REST,8}, {NOTE_A4,8}, {NOTE_B4,8}, {NOTE_CS5,8}, {NOTE_D5,8}, {NOTE_B4,8},//29
        {REST,8}, {NOTE_CS5,8}, {NOTE_B4,8}, {NOTE_A4,-4}, {REST,4},
        {NOTE_B4,8}, {NOTE_B4,8}, {NOTE_CS5,8}, {NOTE_D5,8}, {NOTE_B4,8}, {NOTE_A4,4}, {REST,8},
        {REST,8}, {NOTE_E5,8}, {NOTE_E5,8}, {NOTE_FS5,4}, {NOTE_E5,-4},
        {NOTE_D5,2}, {NOTE_D5,8}, {NOTE_E5,8}, {NOTE_FS5,8}, {NOTE_E5,4},
        {NOTE_E5,8}, {NOTE_E5,8}, {NOTE_FS5,8}, {NOTE_E5,8}, {NOTE_A4,8}, {NOTE_A4,4},

        {REST,-4}, {NOTE_A4,8}, {NOTE_B4,8}, {NOTE_CS5,8}, {NOTE_D5,8}, {NOTE_B4,8},//35
        {REST,8}, {NOTE_E5,8}, {NOTE_FS5,8}, {NOTE_E5,-4}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_FS5,-8}, {NOTE_FS5,-8}, {NOTE_E5,-4}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_E5,-8}, {NOTE_E5,-8}, {NOTE_D5,-8}, {NOTE_CS5,16}, {NOTE_B4,8}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_D5,4}, {NOTE_E5,8}, {NOTE_CS5,-8}, {NOTE_B4,16}, {NOTE_A4,4}, {NOTE_A4,8},

        {NOTE_E5,4}, {NOTE_D5,2}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},//40
        {NOTE_FS5,-8}, {NOTE_FS5,-8}, {NOTE_E5,-4}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_A5,4}, {NOTE_CS5,8}, {NOTE_D5,-8}, {NOTE_CS5,16}, {NOTE_B4,8}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_D5,4}, {NOTE_E5,8}, {NOTE_CS5,-8}, {NOTE_B4,16}, {NOTE_A4,4}, {NOTE_A4,8},
        {NOTE_E5,4}, {NOTE_D5,2}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},

        {NOTE_FS5,-8}, {NOTE_FS5,-8}, {NOTE_E5,-4}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},//45
        {NOTE_A5,4}, {NOTE_CS5,8}, {NOTE_D5,-8}, {NOTE_CS5,16}, {NOTE_B4,8}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_D5,4}, {NOTE_E5,8}, {NOTE_CS5,-8}, {NOTE_B4,16}, {NOTE_A4,4}, {NOTE_A4,8},
        {NOTE_E5,4}, {NOTE_D5,2}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_FS5,-8}, {NOTE_FS5,-8}, {NOTE_E5,-4}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},//45

        {NOTE_A5,4}, {NOTE_CS5,8}, {NOTE_D5,-8}, {NOTE_CS5,16}, {NOTE_B4,8}, {NOTE_A4,16}, {NOTE_B4,16}, {NOTE_D5,16}, {NOTE_B4,16},
        {NOTE_D5,4}, {NOTE_E5,8}, {NOTE_CS5,-8}, {NOTE_B4,16}, {NOTE_A4,4}, {NOTE_A4,8},

        {NOTE_E5,4}, {NOTE_D5,2}, {REST,4},
    };

    static constexpr melody_t melodies[] = {
        {marioMelody, sizeof(marioMelody) / sizeof(melody_note_t), 88},
        {zeldaMelody, sizeof(zeldaMelody) / sizeof(melody_note_t), 88},
        {tetrisMelody, sizeof(tetrisMelody) / sizeof(melody_note_t), 144},
        {starWarsMelody, sizeof(starWarsMelody) / sizeof(melody_note_t), 108},
        {happyBirthdayMelody, sizeof(happyBirthdayMelody) / sizeof(melody_note_t), 100},
        {nevergonnagiveyouupMelody, sizeof(nevergonnagiveyouupMelody) / sizeof(melody_note_t), 114},
    };
    static constexpr size_t melodiesLength = sizeof(melodies) / sizeof(melody_t);

    static constexpr uint8_t BUZZER_PIN = 8;

    const melody_note_t* currentMelody_;
    size_t currentMelodyLenght_;
    unsigned int currentNote_;
    unsigned long nextTick_;
    int beepCount_;
    uint16_t beepFreq_;
    uint16_t beepTimeOn_;
    uint16_t beepTimeOff_;
    int wholeNote_;
    uint8_t currentMelodyIndex_;

    void playMelody(const melody_note_t* melody, size_t lenght, int tempo);
};

#endif
