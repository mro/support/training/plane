#ifndef __THERMISTOR_H_
#define __THERMISTOR_H_

#include <Arduino.h>

class Thermistor
{
public:
    /**
     * Gets raw ADC value for NTC
     * @return NTC raw ADC value
    */
    inline static int getNTCRaw() { return analogRead(TEMP_SENSE_PIN); }

    /**
     * Gets NTC resistance
     * @return Measured NTC resistance [Ohm]
    */
    static float getNTCResistance();

    /**
     * Gets NTC temperature
     * @param resistance Measured resistance
     * @return Measured temperature [C]
    */
    static float getNTCTemperature(float resistance = NAN);

private:
    static constexpr uint8_t TEMP_SENSE_PIN = PIN_A7;   //Temperature analog input
    typedef struct { float resistance; float celsius; } temp_entry_t;
    static constexpr int ADC_RANGE = 1024;          // ADC maximum value
    static constexpr float NTC_REF_RES = 10000.0;   // NTC reference resistance
    //NTC response table (resistance vs temperature), taken from datasheet
    static constexpr temp_entry_t ntcTable[] PROGMEM = {
        { 39204.0f, -10.0f },
        { 37547.0f, -9.0f },
        { 35971.0f, -8.0f },
        { 34472.0f, -7.0f },
        { 33045.0f, -6.0f },
        { 31686.0f, -5.0f },
        { 30391.0f, -4.0f },
        { 29158.0f, -3.0f },
        { 27982.0f, -2.0f },
        { 26861.0f, -1.0f },
        { 25791.0f, 0.0f },
        { 24770.0f, 1.0f },
        { 23796.0f, 2.0f },
        { 22865.0f, 3.0f },
        { 21976.0f, 4.0f },
        { 21127.0f, 5.0f },
        { 20315.0f, 6.0f },
        { 19538.0f, 7.0f },
        { 18796.0f, 8.0f },
        { 18085.0f, 9.0f },
        { 17405.0f, 10.0f },
        { 16753.0f, 11.0f },
        { 16129.0f, 12.0f },
        { 15532.0f, 13.0f },
        { 14959.0f, 14.0f },
        { 14410.0f, 15.0f },
        { 13883.0f, 16.0f },
        { 13378.0f, 17.0f },
        { 12893.0f, 18.0f },
        { 12428.0f, 19.0f },
        { 11982.0f, 20.0f },
        { 11553.0f, 21.0f },
        { 11141.0f, 22.0f },
        { 10746.0f, 23.0f },
        { 10365.0f, 24.0f },
        { 10000.0f, 25.0f },
        { 9649.0f, 26.0f },
        { 9311.0f, 27.0f },
        { 8986.0f, 28.0f },
        { 8673.0f, 29.0f },
        { 8373.0f, 30.0f },
        { 8083.0f, 31.0f },
        { 7804.0f, 32.0f },
        { 7536.0f, 33.0f },
        { 7277.0f, 34.0f },
        { 7028.0f, 35.0f },
        { 6788.0f, 36.0f },
        { 6557.0f, 37.0f },
        { 6334.0f, 38.0f },
        { 6120.0f, 39.0f },
        { 5912.0f, 40.0f },
    };
    static constexpr int ntcTableSize = sizeof(ntcTable) / sizeof(ntcTable[0]);

    Thermistor() = delete;
    virtual ~Thermistor() = delete;
};

#endif
