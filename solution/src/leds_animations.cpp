#include "leds_animations.hpp"
#include <Arduino.h>

#include "ldr.h"
#include "thermistor.hpp"

LEDAnimations::LEDAnimations(LEDs& leds, MPU6050& mpu) : leds_{leds}, mpu_(mpu),
                            prevFlash_{0}, prevLDRRead_{0}, wingsEnabled_(true),
                            ldrEnabled_{true}, mpuEnabled_{true}, tempEnabled_{true}
{
}

void LEDAnimations::begin()
{
}

void LEDAnimations::loop()
{
    unsigned long now = millis();

    if(tempEnabled_ && (Thermistor::getNTCTemperature() >= temperatureThreshold)){
        leds_.setLeds(255, 100, 0, true);
        return;
    }

    //Manage flash
    if(wingsEnabled_){
        unsigned long timeSinceFlash = now - prevFlash_;
        if(timeSinceFlash >= (flashDuration + flashPeriod)){
            //Flash done
            prevFlash_ = now;
        }else if(timeSinceFlash >= flashPeriod){
            leds_.setLed(LEDs::LD5, wingsBrightness, wingsBrightness, wingsBrightness, false);
            leds_.setLed(LEDs::LD6, wingsBrightness, wingsBrightness, wingsBrightness, false);
        }else{
            leds_.setLed(LEDs::LD5, 255, 0, 0, false);
            leds_.setLed(LEDs::LD6, 0, 140, 0, false);
        }
    }else{
        leds_.setLed(LEDs::LD5, 0, 0, 0, false);
        leds_.setLed(LEDs::LD6, 0, 0, 0, false);
    }

    //LDR readout
    if(ldrEnabled_){
        if((now - prevLDRRead_) >= ldrReadPeriod){
            float ldrPercent = LDR::getLight();
            if(ldrPercent < ldrThreshold){
                uint8_t ledPC = (uint8_t)(((ldrThreshold - ldrPercent) / 100.0f) * 128);
                leds_.setLed(LEDs::LD4, ledPC, ledPC, ledPC, false);
            }else{
                leds_.setLed(LEDs::LD4, 0, 0, 0, false);
            }
            prevLDRRead_ = now;
        }
    }else{
        leds_.setLed(LEDs::LD4, 0, 0, 0, false);
    }

    //MPU readout
    if(mpuEnabled_){
        if((now - prevMPURead_) >= mpuReadPeriod){
            float angleY = mpu_.getAngleY();
            if(angleY < 0.0f){
                if(angleY > -1){
                    angleY = 0.0f;
                }
                leds_.setLed(LEDs::LD2, (uint8_t)(-1.0 * angleY * 2.55), 0, 0, false);
                leds_.setLed(LEDs::LD3, 0, 0, 0, false);
            }else{
                leds_.setLed(LEDs::LD2, 0, 0, 0, false);
                if(angleY < 1){
                    angleY = 0.0f;
                }
                leds_.setLed(LEDs::LD3, 0, (uint8_t)(angleY * 2.55), 0, false);
            }

            int angleZ = abs(((int)mpu_.getAngleZ()) % 360);
            leds_.setLedHSV(LEDs::LD1, angleZ, 1, 0.2, false);
            prevMPURead_ = now;
        }
    }else{
        leds_.setLed(LEDs::LD1, 0, 0, 0, false);
        leds_.setLed(LEDs::LD2, 0, 0, 0, false);
        leds_.setLed(LEDs::LD3, 0, 0, 0, false);
    }

    leds_.updateLeds();
}
