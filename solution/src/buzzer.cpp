#include "buzzer.hpp"
#include "pitches.h"
#include <Arduino.h>

constexpr Buzzer::melody_note_t Buzzer::marioMelody[] PROGMEM;
constexpr Buzzer::melody_note_t Buzzer::starWarsMelody[] PROGMEM;
constexpr Buzzer::melody_note_t Buzzer::happyBirthdayMelody[] PROGMEM;
constexpr Buzzer::melody_note_t Buzzer::zeldaMelody[] PROGMEM;
constexpr Buzzer::melody_note_t Buzzer::tetrisMelody[] PROGMEM;
constexpr Buzzer::melody_note_t Buzzer::nevergonnagiveyouupMelody[] PROGMEM;

constexpr Buzzer::melody_t Buzzer::melodies[];

Buzzer::Buzzer() : currentMelody_{nullptr}, currentMelodyLenght_(0),
                    currentNote_{0}, nextTick_{0},
                    beepCount_{-1}, beepFreq_{0}, beepTimeOn_{0}, beepTimeOff_{0},
                    wholeNote_{0}, currentMelodyIndex_{0}
{
}

void Buzzer::begin()
{
    pinMode(BUZZER_PIN, OUTPUT);        //Buzzer is an output
}

bool Buzzer::loop()
{
    if(beepCount_ > 0){
        //Do a beep
        unsigned long now = millis();
        if(now >= nextTick_){
            tone(BUZZER_PIN, beepFreq_, beepTimeOn_);
            nextTick_ = now + beepTimeOn_ + beepTimeOff_;
            --beepCount_;
        }
    }else if((currentMelody_ != nullptr) && (currentMelodyLenght_ > 0)){
        unsigned long now = millis();
        if(now >= nextTick_){
            noTone(BUZZER_PIN);
            uint16_t note = pgm_read_word(&currentMelody_[currentNote_].note);
            int16_t duration = pgm_read_word(&currentMelody_[currentNote_].duration);
            int noteDuration = (wholeNote_) / duration;
            if(duration < 0){
                noteDuration = wholeNote_ / abs(duration);
                noteDuration *= 1.5;
            }
            tone(BUZZER_PIN, note, noteDuration * 0.9);
            if(++currentNote_ >= currentMelodyLenght_){
                //Stop playing
                currentMelodyLenght_ = 0;
                currentMelody_ = nullptr;
            }else{
                nextTick_ = now + noteDuration;
            }

        }
    }
    return (currentMelody_ != nullptr) || (beepCount_ > 0);
}

void Buzzer::playMelody(const melody_note_t* melody, size_t lenght, int tempo)
{
    wholeNote_ = (60000 * 4) / tempo;
    currentMelody_ = melody;
    currentMelodyLenght_ = lenght;
    currentNote_ = 0;
    nextTick_ = 0;
}

void Buzzer::beep(int count, uint16_t freq, uint16_t timeOn, uint16_t timeOff)
{
    //Stop any running music
    currentMelody_ = nullptr;
    currentMelodyLenght_ = 0;
    beepCount_ = count;
    beepFreq_ = freq;
    beepTimeOn_ = timeOn;
    beepTimeOff_ = timeOff;
    nextTick_ = 0;
}

void Buzzer::playNext()
{
    play(currentMelodyIndex_);
    ++currentMelodyIndex_;
    if(currentMelodyIndex_ >= melodiesLength){
        currentMelodyIndex_ = 0;
    }
}

void Buzzer::play(uint8_t index)
{
    if(index < melodiesLength){
        playMelody(melodies[index].melody, melodies[index].lenght, melodies[index].tempo);
    }
}

void Buzzer::stop()
{
    currentMelody_ = nullptr;
    currentMelodyLenght_ = 0;
}
