#include "thermistor.hpp"

constexpr Thermistor::temp_entry_t Thermistor::ntcTable[] PROGMEM;

float Thermistor::getNTCResistance()
{
    int adcRaw = getNTCRaw();
    return -1.0f * ((adcRaw * NTC_REF_RES) / (adcRaw - ADC_RANGE));
}

float Thermistor::getNTCTemperature(float resistance)
{
    if(isnanf(resistance)){
        resistance = getNTCResistance();
    }
    int startIndex = 0;
	int midIndex = ntcTableSize / 2;
	int endIndex = ntcTableSize - 1;

	for(int i = 0; i < ntcTableSize; ++i){
        float midRes = pgm_read_float(&ntcTable[midIndex].resistance);
        if(resistance > midRes){
			//Lower window
			endIndex = midIndex;
		}else{
			//Upper window
			startIndex = midIndex;
		}
        midIndex = ((endIndex - startIndex) / 2) + startIndex;
		if((endIndex - startIndex) < 2){
			break;
		}
	}
    float ratio = 0.0;
    float offset = 0.0;
    float startRes = pgm_read_float(&ntcTable[startIndex].resistance);
    float startTemp = pgm_read_float(&ntcTable[startIndex].celsius);
    float endRes = pgm_read_float(&ntcTable[endIndex].resistance);
    float endTemp = pgm_read_float(&ntcTable[endIndex].celsius);
    ratio = (endTemp - startTemp) / (endRes - startRes);
    offset = startTemp - ratio * startRes;
    return resistance * ratio + offset;
}
