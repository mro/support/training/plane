#include <Arduino.h>
#include "leds.hpp"
#include "buzzer.hpp"
#include "leds_animations.hpp"
#include "button.hpp"
#include "thermistor.hpp"

#include "Wire.h"
#include <MPU6050_light.h>

MPU6050 mpu(Wire);
unsigned long timer = 0;

//Reference to our plane
LEDs leds;
Button button;
Buzzer buzzer;
LEDAnimations ledAnims(leds, mpu);

/**
 * Error state
*/
void error() {
  while(1){
    bool running = buzzer.loop();
    if(!running){
      buzzer.beep(2, 220, 300, 500);
    }else{
      return;
    }
  }
}

void setup() {
  // Initialize serial communication
  Serial.begin(9600);
  // Starts I2C for MPU
  Wire.begin();
  // Initialize LEDs
  leds.begin();
  // Initialize button press
  button.begin();
  // Initialize buzzer
  buzzer.begin();

  // Initialize MPU and go to error state if not found
  byte status = mpu.begin(3);
  if(status != 0){
    error();
  }
  mpu.upsideDownMounting = true; // uncomment this line if the MPU6050 is mounted upside-down
  mpu.calcOffsets(); // gyro and accelero

  // Initialize LED animations
  ledAnims.begin();
}

unsigned long lastTempRead_ = 0;

void loop() {
  // //Fetch data from accelerometer
  mpu.update();

  buzzer.loop();
  int press = button.loop();

  if((press == 0) && (Serial.available() > 0)){
      String rcv = Serial.readString();
      press = rcv.toInt();
  }

  switch (press)
  {
    case 1:
      buzzer.stop();
      buzzer.playNext();
      break;
    case 2:
      ledAnims.setTemperatureEnabled(!ledAnims.temperatureEnabled());
      break;
    case 3:
      ledAnims.setMPUEnabled(!ledAnims.mpuEnabled());
      break;
    case 4:
      ledAnims.setLDREnabled(!ledAnims.ldrEnabled());
     break;
    case 5:
      ledAnims.setWingsEnabled(!ledAnims.wingsEnabled());
      break;
    default:
      break;
  }
  if(press > 5){
    buzzer.beep(2, 1500, 100);
  }else if(press > 1){
    buzzer.beep(press, 800);
  }

  //Loop the led animation
  ledAnims.loop();

  //Read the thermistor
  unsigned long now = millis();
  if((now - lastTempRead_) > 5000){
    Serial.print("Temperature : ");
    Serial.print(Thermistor::getNTCTemperature());
    Serial.println("C");
    lastTempRead_ = now;
  }
}
