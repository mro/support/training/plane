#include "button.hpp"

Button::Button() : nbPress_{0}, lastPress_{0}
{
}

/**
 * Configure the button
*/
void Button::begin()
{
    pinMode(BUTTON_PIN, INPUT_PULLUP);
}

int Button::loop()
{
    bool state = getState();
    unsigned long now = millis();

    if(((now - lastPress_) > PRESS_TIMEOUT) && (lastPress_ > 0)){
        lastPress_ = 0;
        int ret = nbPress_;
        nbPress_ = 0;
        return ret;
    }

    if((prevState_ != state) && state){
        //Rising edge on button
        if((now - lastPress_) > DEBOUNCE_TIME){
            //Take it into account
            ++nbPress_;
            lastPress_ = now;
        }
    }
    prevState_ = state;
    return 0;
}
