# Exercise 3 : bit bang leds

The objective of this execice is to:
- find an opensource library for your hardware.
- implement a state in an application.
- leverage transforms to manipulate values (HSV vs RGB).

# Leds and libraries

Even leds are now components that you can communicate with, the leds being used on this project
are [WS2812B](https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf) devices.

Check how those leds are connected and how it's meant to work. 🤔

For this exercise we decided to use an available driver for this component ([FastLED](http://fastled.io)),
while the final solution uses [FabLED](https://github.com/sonyhome/FAB_LED/).

Have a look into both projects, make your own review and opinion, evaluating libraries
and dependencies is part of a developer's job !

**Hint:** to install a library from VSCode: ![installing library](../docs/FastLED_library.png).

This project should compile and upload once this dependency is installed, so
you can focus on the application and on Things to do !

## Things to do

1. Install the FastLED library, compile, and upload the project.
2. Implement a rainbow animation on the leds (see `rotateHSV` in [src/main.cpp](src/main.cpp) and `LEDs::setLedHSV`, read some documentation about [HSV](https://en.wikipedia.org/wiki/HSL_and_HSV)).
3. Transform `fadeRedLed` to fade either the red, the green or blue led (add an argument).
3. Add back your button driver and switch from one animation to another on key-press.

## C++ tips and Tricks

**Hint**: A variable declared `static` in a function will be initialised on first call and persist its value in later function calls.

**Hint**: A global variable is declared outside of any function, it is quite similar to `static` described above and will persist its state accorss the entire program.

**Hint**: The `++` and `--` operators increment a variable and return its (previous) value, ex: `red++`.

**Hint**: equality comparison in C++ is `==`, not `=`, example:
```c++
/* global variable, will persist and is available to all functions below declaration */
int state = 0;

void loop()
{
  /* mind the `==` */
  if (state == 0)
    state = 1;
  else
    state = 0;
}
```

[Next Exercise](../04_gyroscope/README.md)
