

#include "leds.hpp"
#include <FastLED.h>

LEDs::LEDs() : ledsDirty_{false} {}

void LEDs::begin() {
  FastLED.addLeds<WS2812B, SMD_LEDS_PIN, RGB>(Leds_, SMD_LEDS_COUNT);
  FastLED.addLeds<WS2812B, THT_LEDS_PIN, GRB>(Leds_ + SMD_LEDS_COUNT,
                                              THT_LEDS_COUNT);
  FastLED.clear();
  ledsDirty_ = true;
}

void LEDs::setLed(uint8_t number, uint8_t red, uint8_t green, uint8_t blue,
                  bool sendNow) {
  if ((number >= 0) && (number < ledsCount())) {
    Leds_[number] = CRGB(red, green, blue);

    ledsDirty_ = true;

    if (sendNow)
      updateLeds();
  }
}

void LEDs::setLeds(uint8_t red, uint8_t green, uint8_t blue, bool sendNow) {
  for (uint8_t i = 0; i < ledsCount(); ++i) {
    setLed(i, red, green, blue, false);
  }
  if (sendNow)
    updateLeds();
}

void LEDs::setLedHSV(uint8_t number, float hue, float saturation, float value,
                     bool sendNow) {
  uint8_t r, g, b = 0;
  HSVToRGB(hue, saturation, value, r, g, b);
  setLed(number, r, g, b, sendNow);
}

void LEDs::setLedsHSV(float hue, float saturation, float value, bool sendNow) {
  uint8_t r, g, b = 0;
  HSVToRGB(hue, saturation, value, r, g, b);
  setLeds(r, g, b, sendNow);
}

void LEDs::updateLeds() {
  if (ledsDirty_) {
    ledsDirty_ = false;
    FastLED.show();
  }
}

void LEDs::HSVToRGB(float hue, float saturation, float value, uint8_t &red,
                    uint8_t &green, uint8_t &blue) {
  float r = 0, g = 0, b = 0;
  if (saturation == 0) {
    r = value;
    g = value;
    b = value;
  } else {
    int i;
    double f, p, q, t;

    if (hue == 360)
      hue = 0;
    else
      hue = hue / 60;

    i = (int)trunc(hue);
    f = hue - i;

    p = value * (1.0 - saturation);
    q = value * (1.0 - (saturation * f));
    t = value * (1.0 - (saturation * (1.0 - f)));

    switch (i) {
    case 0:
      r = value;
      g = t;
      b = p;
      break;

    case 1:
      r = q;
      g = value;
      b = p;
      break;

    case 2:
      r = p;
      g = value;
      b = t;
      break;

    case 3:
      r = p;
      g = q;
      b = value;
      break;

    case 4:
      r = t;
      g = p;
      b = value;
      break;

    default:
      r = value;
      g = p;
      b = q;
      break;
    }
  }

  red = (uint8_t)(r * 255);
  green = (uint8_t)(g * 255);
  blue = (uint8_t)(b * 255);
}
