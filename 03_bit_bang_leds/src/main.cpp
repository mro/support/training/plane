#include "leds.hpp"
#include <Arduino.h>

// Reference to our plane
LEDs leds;

void setup() {
  // Initialize serial communication
  Serial.begin(9600);

  // Initialize LEDs
  leds.begin();
}

/**
 * Example application
 */
void fadeRedLed() {
  static unsigned long lastUpdate = 0;
  static int red = 0;
  static bool inc = true;

  unsigned long now = millis();
  if (now > lastUpdate + 10) {
    if (inc) {
      leds.setLeds(red++, 0, 0);
      inc = (red < 255);
    } else {
      leds.setLeds(red--, 0, 0);
      inc = !(red > 0);
    }
    lastUpdate = now;
  }
}

void loop() {
  // TODO: fade a different color on each button press

  fadeRedLed();
  leds.updateLeds();
}
